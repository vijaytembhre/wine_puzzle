<?php
ini_set('memory_limit', '4000M');
set_time_limit(0);

class Winepuzz {

    // input file name to import data      
    public $InpName;

    function __construct($nameF) {
        $this->InpName = $nameF;
    }

    //person and wine id file assign to this class to process and output result

    public function PersonWines() {
        $finalList = [];
        $INpfile = fopen($this->InpName, "r");
        $aggregateSoldttl = 0;
        $wineChoosOpt = [];
        $wineListAll = [];

        while (($line = fgets($INpfile)) !== false) {
            $pname_wine_cod = explode("\t", $line);
            $nameP = trim($pname_wine_cod[0]);
            $wineCodd = trim($pname_wine_cod[1]);
            if (!array_key_exists($wineCodd, $wineChoosOpt)) {
                $wineChoosOpt[$wineCodd] = [];
            }
            $wineChoosOpt[$wineCodd][] = $nameP;
            $wineListAll[] = $wineCodd;
        }
        fclose($INpfile);
        $wineListAll = array_unique($wineListAll);
        foreach ($wineListAll as $key => $wineCodd) {
                $personNCod = $wineChoosOpt[$wineCodd][0];
                if (!array_key_exists($personNCod, $finalList)) {
                    $finalList[$personNCod] = [];
                }
                if (count($finalList[$personNCod]) < 3) {
                    $finalList[$personNCod][] = $wineCodd;
                    $aggregateSoldttl++;
                }
        }

        $fh = fopen("FinalRess.txt", "w");
        fwrite($fh, "Sum of wine bottles sold in aggregate : " . $aggregateSoldttl . " \n");
        foreach (array_keys($finalList) as $key => $personNCod) {
            foreach ($finalList[$personNCod] as $key => $wineCodd) {
                fwrite($fh, $personNCod . " " . $wineCodd . "\n");
            }
        }
        fclose($fh);
    }

}

$winepuzzle = new Winepuzz("person_wine_3.txt");
$winepuzzle->PersonWines();
?>
